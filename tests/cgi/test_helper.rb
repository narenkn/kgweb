# Here's the trick:
# We need to run the Webrick server in a separate thread so the
# testcases can make requests and block, waiting for the response.
WEBRICK = Thread.new do
  require 'webrick'

  server = WEBrick::HTTPServer.new(
    :Port => 8999,
    :DocumentRoot => File.expand_path("..", __FILE__),
  )
  cgi_scr = File.expand_path(File.dirname(__FILE__) + "/../cgi.rb")
  puts cgi_scr;
  server.mount("/cgi", WEBrick::HTTPServlet::CGIHandler, cgi_scr)
  trap('INT') { server.shutdown }

  puts "Starting Webrick on port 8999"
  server.start
end

# give webrick time to boot before the tests can run
sleep 1
