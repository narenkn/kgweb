require 'rubygems'
require 'webrick'
require 'json'
require 'time'

class WebForm < WEBrick::HTTPServlet::AbstractServlet

  @@orders = [];
  @@itemsInKitchen = {};
  @@bill = {};
  @@foots = {};
  @@orderId = 0;

  def orders2Json
    @@itemsInKitchen = {};
    @@orders.each do |order|
      order['items'].each do |item|
        if not item.has_key?('status')
          item['status'] = 'kitchen';
        else
          item['status'] = 'delivered';
        end
      end
    end
    @@orders.each do |order|
      order['items'].each do |item|
        next if ('kitchen' != item['status']);
        @@itemsInKitchen[item['name']] ||= 0;
        @@itemsInKitchen[item['name']] += 1;
      end
    end
  end

  def orders2bill
    itemsDelivered = {};
    @@bill = [];
    @@orders.each do |order|
      order['items'].each do |item|
        ##next if ('kitchen' != item['status']);
        itemsDelivered[item['name']] ||= 0;
        itemsDelivered[item['name']] += item['quantity'];
        if @@bill.any? {|it| it['name'] == item['name']}
          ii = @@bill.index {|it| it['name'] == item['name']}
          @@bill[ii]['quantity'] += item['quantity'];
        else
          @@bill << item;
        end
      end
    end
    total = 0;
    @@bill.each do |item|
      puts item;
      total += item['quantity'] * item['price'];
    end
    @@foots = [ {'name': 'CGST', 'value': 101}, {'name': 'SGST', 'value': 51}, {'name': 'Total', 'value': total} ];
  end

  def do_GET(request, response)
    status, content_type, body = get_answer('GET', request)

    response.status = status
    response['Content-Type'] = content_type
    response.body = body
  end

  def do_POST(request, response)
    status, content_type, body = get_answer('POST', request)
    
    response.status = status
    response['Content-Type'] = content_type
    response.body = body
  end

  def do_PUT(request, response)
    status, content_type, body = get_answer('PUT', request)
    
    response.status = status
    response['Content-Type'] = content_type
    response.body = body
  end

  def do_DELETE(request, response)
    status, content_type, body = get_answer('DELETE', request)
    
    response.status = status
    response['Content-Type'] = content_type
    response.body = body
  end

  def do_PATCH(request, response)
    status, content_type, body = get_answer('PATCH', request)
    
    response.status = status
    response['Content-Type'] = content_type
    response.body = body
  end

  def get_answer(type, request)
    otype = "text/html";
    resp = '';
    if ('POST' == type) and (request.path =~ /\/cgi\/calendar.json/)
      otype = 'application/json';
      resp = %{
[
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-01-01"
	},
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-01-03"
	},
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-01-22",
            "end": "2019-01-23"
	},
	{
            "id": "999",
            "title": "Repeating Event",
            "url": "http://google.com/",
            "start": "2019-01-23T16:00:00+00:00"
	},
	{
            "id": "999",
            "title": "Repeating Event",
            "url": "http://google.com/",
            "start": "2019-01-23T16:00:00+00:00"
	},
	{
            "title": "Conference",
            "start": "2019-01-11",
            "end": "2019-01-13"
	},
	{
            "title": "Meeting",
            "start": "2019-01-12T10:30:00+00:00",
            "end": "2019-01-12T12:30:00+00:00"
	},
	{
            "title": "Lunch",
            "start": "2019-01-12T12:00:00+00:00"
	},
	{
            "title": "Meeting",
            "start": "2019-01-12T14:30:00+00:00"
	},
	{
            "title": "Dinner",
            "start": "2019-01-12T20:00:00+00:00"
	},
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-02-02"
	},
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-02-04"
	},
	{
            "title": "Click for Google",
            "url": "http://google.com/",
            "start": "2019-02-22",
            "end": "2019-02-23"
	},
	{
            "id": "999",
            "title": "Repeating Event",
            "url": "http://google.com/",
            "start": "2019-02-23T16:00:00+00:00"
	},
	{
            "id": "999",
            "title": "Repeating Event",
            "url": "http://google.com/",
            "start": "2019-02-23T16:00:00+00:00"
	},
	{
            "title": "Conference",
            "start": "2019-02-11",
            "end": "2019-02-13"
	},
	{
            "title": "Meeting",
            "start": "2019-02-12T10:30:00+00:00",
            "end": "2019-02-12T12:30:00+00:00"
	}
    ]
};
    elsif ('POST' == type) and (request.path =~ /\/cgi\/timetable.json/)
      otype = 'application/json';
      resp = %{ [
                      {
                        "title": "Mathematics 08:00 - 12:30",
                        "start": "2019-04-15T08:00:00+00:00",
                        "end": "2019-04-15T12:30:00+00:00",
                        "className": "mathematics"
                      },
                      {
                        "title": "Mathematics 09:00 - 11:00",
                        "start": "2019-04-20T09:00:00+00:00",
                        "end": "2019-04-20T11:00:00+00:00",
                        "className": "mathematics"
                      },
                      {
                        "title": "Mathematics 13:00 - 16:00",
                        "start": "2019-04-16T13:00:00+00:00",
                        "end": "2019-04-16T16:00:00+00:00",
                        "className": "mathematics"
                      },
                      {
                        "title": "Learning Class 08:00 - 09:00",
                        "start": "2019-04-17T08:00:00+00:00",
                        "end": "2019-04-17T09:00:00+00:00",
                        "className": "learning"
                      },
                      {
                        "title": "Learning Class 11:00 - 13:00",
                        "start": "2019-04-16T11:00:00+00:00",
                        "end": "2019-04-16T13:00:00+00:00",
                        "className": "learning"
                      },
                      {
                        "title": "Learning Class 16:00 - 19:00",
                        "start": "2019-04-20T16:00:00+00:00",
                        "end": "2019-04-20T19:00:00+00:00",
                        "className": "learning"
                      },
                      {
                        "title": "Language Class 14:00 - 18:00",
                        "start": "2019-04-16T14:00:00+00:00",
                        "end": "2019-04-16T18:00:00+00:00",
                        "className": "language"
                      },
                      {
                        "title": "Language Class 11:00 - 14:30",
                        "start": "2019-04-19T11:00:00+00:00",
                        "end": "2019-04-19T14:30:00+00:00",
                        "className": "language"
                      },
                      {
                        "title": "Drawing Class 10:00 - 12:00",
                        "start": "2019-04-17T10:00:00+00:00",
                        "end": "2019-04-17T12:00:00+00:00",
                        "className": "drawing"
                      },
                      {
                        "title": "Drawing Class 16:30 - 8:30:",
                        "start": "2019-04-19T16:30:00+00:00",
                        "end": "2019-04-19T18:30:00+00:00",
                        "className": "drawing"
                      },
                      {
                        "title": "Drawing Class 11:30 - 13:00",
                        "start": "2019-04-14T11:30:00+00:00",
                        "end": "2019-04-14T13:00:00+00:00",
                        "className": "drawing"
                      },
                      {
                        "title": "Multimedia Class 14:00 - 16:30",
                        "start": "2019-04-15T14:00:00+00:00",
                        "end": "2019-04-15T16:30:00+00:00",
                        "className": "multimedia"
                      },
                      {
                        "title": "Multimedia Class 17:00 - 19:00",
                        "start": "2019-04-17T17:00:00+00:00",
                        "end": "2019-04-17T19:00:00+00:00",
                        "className": "multimedia"
                      },
                      {
                        "title": "Multimedia Class 08:00 - 10:30",
                        "start": "2019-04-15T08:00:00+00:00",
                        "end": "2019-04-15T10:30:00+00:00",
                        "className": "multimedia"
                      }
                    ]
};
    end

    puts 'get_answer:', resp;
    return 200, otype, resp;
  end
end

if $0 == __FILE__ then
  server = WEBrick::HTTPServer.new(:Port => 8080, :DocumentRoot => Dir.pwd)
  server.mount "/cgi", WebForm
  server.mount "/", WEBrick::HTTPServlet::FileHandler, './www/'
  trap "INT" do server.shutdown end
  server.start
end
