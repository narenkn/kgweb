
var ngApp = angular.module('SchoolApp', ['ngSanitize', 'ngRoute']);

ngApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
        when('/home', {templateUrl: 'home.html', activetab: 'home'}).
        when('/calendar', {templateUrl: 'calendar.html', activetab: 'calendar'}).
        when('/about', {templateUrl: 'about.html', activetab: 'about'}).
        when('/contact', {templateUrl: 'contact.html', activetab: 'contact'}).
        when('/courses', {templateUrl: 'courses.html', activetab: 'courses'}).
        when('/admins', {templateUrl: 'admin.html', activetab: 'admins'}).
        when('/timetable', {templateUrl: 'timetable.html', activetab: 'timetable'}).
        otherwise({redirectTo: '/home'});
    $locationProvider.html5Mode(true);
}]);

ngApp.controller('SchoolController', ['$scope', '$route', '$sce', function ($scope, $route, $sce) {
    $scope.$route = $route;

    $scope.checkSchoolStatus = function() {
	var open = new Date(); open.setHours(9,0,0,0);
	var close = new Date(); close.setHours(15,30,0,0);
	var today = new Date();
	if ((today >= open) && (today <= close) && (today.getDay() != 0)) {
	    return true;
	}
	return false;
    };

    $scope.getLogo = function() {
	var logo='Great Human International Model School';
	var caption='Identify & Cherish Geniusness !';
	var ht = '<div style="font-family:\'Georgia\', cursive;font-size:45px;line-height:47px;font-style: italic;font-variant: small-caps; color: #349A54; font-weight: bold">';
	ht += logo;
	ht += '</div><div style="font-family:\'Georgia\', cursive;font-size:15px;line-height:15px;font-style: italic">';
	ht += caption;
	ht += '<div style="font-family:\'Georgia\', cursive; font-style: italic;font-variant: small-caps; font-weight: bold">';
	return $sce.trustAsHtml(ht);
    };

    $scope.PageData = {
	school : {
	    name : 'Great Human International Model School',
	    nametag : 'Play School, Montessori School,Primary School, Nursery School, Pre School, Play Way School,  Nursery & Primary School, Kids Education & Kindergarten School',
	    keywords : 'Play School, Pre School, Play Way School, Montessori School, Kids Education & Kindergarten School, kindergarten, kindergarden, children, kids school,school,baby,child school,academy,course,education, Best School in Udumalpet, Best Montessori School in Udumalpet, Best Play School in Udumalpet, Best Primary School in Udumalpet, Best International School in Udumalpet, Alternative School',
	    office : '+91 91591 88222, +91 91591 55222',
	    phone : '+91 4252 222955',
	    fax : null,
	    openHours : {
		'Monday-Saturday' : '9:30 AM -5:30 PM',
		'Sunday' : 'Closed',
	    },
	    address : '220, Syed Fakrudeen Street, Gandhi Nagar, Udumalpet - 642154',
	    mail : 'greathuman.school@gmail.com',
	    domain : 'www.ghims.com',
	    lat: "-37.817314",
	    lng: "144.955431",
	},
	home : {
	    images : ['images/bg/bg1.jpg', 'images/bg/bg2.jpg', 'images/bg/bg4.jpg'],
	    data : ['Best Children Kindergarten',
		    "We provides always our best industrial solution for our clients <br>and  always try to achieve our client's trust and satisfaction.",
		    'Kinder', 'Garten'],
	},
	about : {
	    title: 'Best Education in Our Kindergarten',
	    h4: 'Lorem ipsum dolor sit amet soluta saepe odit error, maxime praesentium sunt udiandae!',
	    p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore atque officiis maxime suscipit expedita obcaecati nulla in ducimus iure quos quam recusandae dolor quas et perspiciatis voluptatum accusantium delectus nisi reprehenderit, eveniet fuga modi pariatur, eius vero. Ea vitae maiores.',
	    funfact: [ {
		h5: 'Qualified Teachers',
		value: 20,
	    }, {
		h5: 'Successful Kids',
		value: 300,
	    }, {
		h5: 'Happy Parents',
		value: 600,
	    }, {
		h5: 'Awards Won',
		value: 10,
	    } ],
	    features: {
		subtext: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!',
		left: [
		    {
			icon: '<i class="fa fa-pencil-square-o text-white"></i>',
			h4: 'Active Learning',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-book text-white"></i>',
			h4: 'Multimedia Class',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-graduation-cap text-white"></i>',
			h4: 'Full Day Programs',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		],
		right: [
		    {
			icon: '<i class="fa fa-graduation-cap text-white"></i>',
			h4: 'Expert Teachers',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-book text-white"></i>',
			h4: 'Funny and Happy',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-pencil-square-o text-white"></i>',
			h4: 'Fulfill With Love',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		],
	    },
	},
	admins: [
	    {
		name: 'Mrs.Saranyaa M.Sc, B.Ed',
		pic: null,
		specialization: 'Principal',
		exps: [
		    'Master of Science (M.Sc), Bachilor of Education (B.Ed)',
		],
		p: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.',
		],
		phone: '+91 96262 55620 ( Only for Academic Grievances, Suggestions & Compliances  )',
		email: 'greathuman.school@gmail.com',
	    },
	    {
		name: 'Mrs.Sabithamani (BA Psychology, RNRM)',
		pic: null,
		specialization: 'Co-Ordinator',
		exps: [
		    '( Bachilor of Arts ) (Psychology), RNRM',
		],
		p: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.',
		],
		phone: '+91 91591 88222 ( Only for Administrative Grievances, Suggestions & Compliances  )',
		email: 'greathuman.school@gmail.com',
	    },
	     {
		name: 'Mrs.Narmatha (MA ( English ), B.Ed)',
		pic: null,
		specialization: 'Co-Ordinator',
		exps: [
		    '( Master of Arts ) (English), B.Ed',
		],
		p: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.',
		],
		phone: '+91 91591 66222 ( Only for Administrative Grievances, Suggestions & Compliances  )',
		email: 'greathuman.school@gmail.com',
	    },
	],
	courses: [
	    {
		h2: 'Lower Kindergarten (LKG)',
		price: null,
		rating: "5.0",
		img: 'images/services/lg1.jpg',
		head: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo unde, <span class="text-theme-color-red">Lower Kindergarten</span> corporis dolorum blanditiis ullam officia <span class="text-theme-color-red">our kindergarten </span>natus minima fugiat repellat! Corrupti voluptatibus aperiam voluptatem. Exercitationem, placeat, cupiditate.',
		body: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		],
		footer: null,
		infos: [
		    { wh: 'Start Date', v: 'Jan 15, 2018'},
		    { wh: 'Years Old', v: '5-6 Years'},
		    { wh: 'Class Size', v: '20-30 Kids'},
		    { wh: 'Staff', v: '2 Teachers'},
		    { wh: 'Carry Time', v: '5 Hours/6 Days'},
		    { wh: 'Duration', v: '10-12 Month'},
		    { wh: 'Class Time', v: '9:30am-5:30pm'},
		],
	    },
	    {
		h2: 'Upper Kindergarten (UKG)',
		price: null,
		rating: "5.0",
		img: 'images/services/lg1.jpg',
		head: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo unde, <span class="text-theme-color-red">Upper Kindergarten</span> corporis dolorum blanditiis ullam officia <span class="text-theme-color-red">our kindergarten </span>natus minima fugiat repellat! Corrupti voluptatibus aperiam voluptatem. Exercitationem, placeat, cupiditate.',
		body: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		],
		footer: null,
		infos: [
		    { wh: 'Start Date', v: 'Jan 15, 2018'},
		    { wh: 'Years Old', v: '5-6 Years'},
		    { wh: 'Class Size', v: '20-30 Kids'},
		    { wh: 'Staff', v: '2 Teachers'},
		    { wh: 'Carry Time', v: '5 Hours/6 Days'},
		    { wh: 'Duration', v: '10-12 Month'},
		    { wh: 'Class Time', v: '9:30am-5:30pm'},
		],
	    },
	],
	timetable: {
	    LKG: {
		googleCalendarId: 'ugiri0ljdsk4o3b8k0s7jdus7g@group.calendar.google.com',
		color: 'red',
	    },
	    UKG: {
		googleCalendarId: 'm8p38j2hel9ne0tviuddadse1s@group.calendar.google.com',
		color: 'blue',
	    },
	},
    };

//    $scope.initialize = function() {
//	for (var o in $scope.PageData.timetable) {
//	    if ($scope.section_selection == o) continue;
//	    $scope.section_selection = o;
//	}
//    };
//    $scope.initialize();

}]);

