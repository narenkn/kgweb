var ngApp = angular.module('SchoolApp', ['ngSanitize', 'ngRoute']);

ngApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
        when('/home', {templateUrl: 'home.html', activetab: 'home'}).
        when('/calendar', {templateUrl: 'calendar.html', activetab: 'calendar'}).
        when('/about', {templateUrl: 'about.html', activetab: 'about'}).
        when('/contact', {templateUrl: 'contact.html', activetab: 'contact'}).
        when('/courses', {templateUrl: 'courses.html', activetab: 'courses'}).
        when('/admins', {templateUrl: 'admin.html', activetab: 'admins'}).
        when('/timetable', {templateUrl: 'timetable.html', activetab: 'timetable'}).
        otherwise({redirectTo: '/home'});
    $locationProvider.html5Mode(true);
}]);

ngApp.controller('SchoolController', ['$scope', '$route', '$sce', function ($scope, $route, $sce) {
    $scope.$route = $route;

    $scope.checkSchoolStatus = function() {
	var open = new Date(); open.setHours(9,0,0,0);
	var close = new Date(); close.setHours(15,30,0,0);
	var today = new Date();
	if ((today >= open) && (today <= close) && (today.getDay() != 0)) {
	    return true;
	}
	return false;
    };

    $scope.getLogo = function() {
	var logo='GHIMS';
	var caption='Knowledge empowers!';
	var ht = '<div style="font-family:\'Baloo Chettan\', cursive;font-size:45px;line-height:47px;font-style: italic">';
	ht += logo;
	ht += '</div><div style="font-family:\'Pacifico\', cursive;font-size:15px;line-height:15px;font-style: italic">';
	ht += caption;
	ht += '</div>';
	return $sce.trustAsHtml(ht);
    };

    $scope.PageData = {
	school : {
	    name : 'Great Human International Model School',
	    nametag : 'Kids Education & Kindergarten School',
	    keywords : 'kindergarten,children,kidsschool,school,baby,childschool,academy,course,education,',
	    office : '+91 1234567890',
	    phone : '+91 1234567890',
	    fax : null,
	    openHours : {
		'Mon-Fri' : '8:30-16:00',
		'Sat' : '10:00-12:30',
		'Sun' : 'Closed',
	    },
	    address : '10, Gandhinagar, Udumalpet - 642126',
	    mail : 'contact@myschool.co.in',
	    domain : 'www.myschool.co.in',
	    lat: "-37.817314",
	    lng: "144.955431",
	},
	home : {
	    images : ['images/bg/bg1.jpg', 'images/bg/bg2.jpg', 'images/bg/bg4.jpg'],
	    data : ['Best Children Kindergarten',
		    "We provides always our best industrial solution for our clients <br>and  always try to achieve our client's trust and satisfaction.",
		    'Kinder', 'Garten'],
	},
	about : {
	    title: 'Best Education in Our Kindergarten',
	    h4: 'Lorem ipsum dolor sit amet soluta saepe odit error, maxime praesentium sunt udiandae!',
	    p: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore atque officiis maxime suscipit expedita obcaecati nulla in ducimus iure quos quam recusandae dolor quas et perspiciatis voluptatum accusantium delectus nisi reprehenderit, eveniet fuga modi pariatur, eius vero. Ea vitae maiores.',
	    funfact: [ {
		h5: 'Qualified Teachers',
		value: 20,
	    }, {
		h5: 'Successful Kids',
		value: 300,
	    }, {
		h5: 'Happy Parents',
		value: 600,
	    }, {
		h5: 'Awards Won',
		value: 10,
	    } ],
	    features: {
		subtext: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!',
		left: [
		    {
			icon: '<i class="fa fa-pencil-square-o text-white"></i>',
			h4: 'Active Learning',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-book text-white"></i>',
			h4: 'Multimedia Class',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-graduation-cap text-white"></i>',
			h4: 'Full Day Programs',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		],
		right: [
		    {
			icon: '<i class="fa fa-graduation-cap text-white"></i>',
			h4: 'Expert Teachers',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-book text-white"></i>',
			h4: 'Funny and Happy',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		    {
			icon: '<i class="fa fa-pencil-square-o text-white"></i>',
			h4: 'Fulfill With Love',
			p: 'GHIMS is a creative skill and a joy beyond anything found',
		    },
		],
	    },
	},
	admins: [
	    {
		name: 'Sakib Smith',
		pic: 'images/team/team-details.jpg',
		specialization: 'Chemistry Teacher',
		exps: [
		    'Chemistry (BSc), Computer Science (BSc), English and Creative Writing (BA)',
		],
		p: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.',
		],
		phone: null,
		email: 'ssmith@yourdomain.com',
	    },
	    {
		name: 'Sakib Smith2',
		pic: null,
		specialization: 'Physics Teacher',
		exps: [
		    'Physics (BSc), Computer Science (BSc), English and Creative Writing (BA)',
		],
		p: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam vero expedita fugiat illo quasi doloremque, in unde omnis sint assumenda! Quaerat in, reprehenderit corporis voluptatum natus sequi reiciendis ullam. Quam eaque dolorum voluptates cupiditate explicabo.',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias, consectetur blanditiis eum maxime sunt accusantium ipsa doloribus reiciendis. Ea quod reprehenderit deserunt. Veritatis omnis similique tempora delectus a consequuntur, quis.  Adipisicing elit. Nesciunt culpa dicta quaerat qui recusandae possimus placeat quidem ipsam voluptates similique libero tempore, labore quasi delectus vero alias.',
		],
		phone: null,
		email: 'ssmith2@yourdomain.com',
	    },
	],
	courses: [
	    {
		h2: 'Lower Kindergarten (LKG)',
		price: null,
		rating: "5.0",
		img: 'images/services/lg1.jpg',
		head: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo unde, <span class="text-theme-color-red">Lower Kindergarten</span> corporis dolorum blanditiis ullam officia <span class="text-theme-color-red">our kindergarten </span>natus minima fugiat repellat! Corrupti voluptatibus aperiam voluptatem. Exercitationem, placeat, cupiditate.',
		body: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		],
		footer: null,
		infos: [
		    { wh: 'Start Date', v: 'Jan 15, 2018'},
		    { wh: 'Years Old', v: '5-6 Years'},
		    { wh: 'Class Size', v: '20-30 Kids'},
		    { wh: 'Staff', v: '2 Teachers'},
		    { wh: 'Carry Time', v: '5 Hours/6 Days'},
		    { wh: 'Duration', v: '10-12 Month'},
		    { wh: 'Class Time', v: '9:30am-5:30pm'},
		],
	    },
	    {
		h2: 'Upper Kindergarten (UKG)',
		price: null,
		rating: "5.0",
		img: 'images/services/lg1.jpg',
		head: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo unde, <span class="text-theme-color-red">Upper Kindergarten</span> corporis dolorum blanditiis ullam officia <span class="text-theme-color-red">our kindergarten </span>natus minima fugiat repellat! Corrupti voluptatibus aperiam voluptatem. Exercitationem, placeat, cupiditate.',
		body: [
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit, inventore aliquid incidunt, quasi error! Natus esse rem eaque asperiores eligendi dicta quidem iure, excepturi doloremque eius neque autem sint error qui tenetur, modi provident aut, maiores laudantium reiciendis expedita. Eligendi',
		],
		footer: null,
		infos: [
		    { wh: 'Start Date', v: 'Jan 15, 2018'},
		    { wh: 'Years Old', v: '5-6 Years'},
		    { wh: 'Class Size', v: '20-30 Kids'},
		    { wh: 'Staff', v: '2 Teachers'},
		    { wh: 'Carry Time', v: '5 Hours/6 Days'},
		    { wh: 'Duration', v: '10-12 Month'},
		    { wh: 'Class Time', v: '9:30am-5:30pm'},
		],
	    },
	],
	timetable: {
	    LKG: {
		googleCalendarId: 'ugiri0ljdsk4o3b8k0s7jdus7g@group.calendar.google.com',
		color: 'red',
	    },
	    UKG: {
		googleCalendarId: 'm8p38j2hel9ne0tviuddadse1s@group.calendar.google.com',
		color: 'blue',
	    },
	},
	contacts: {
	},
    };

//    $scope.initialize = function() {
//	for (var o in $scope.PageData.timetable) {
//	    if ($scope.section_selection == o) continue;
//	    $scope.section_selection = o;
//	}
//    };
//    $scope.initialize();

}]);
