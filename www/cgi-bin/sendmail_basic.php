#! /bin/env php

<?php

header('Content-Type: application/json');
error_reporting( E_ALL );
ini_set('display_errors', 1);

parse_str(urldecode(file_get_contents('php://stdin')), $post);

$sender = $post["form_email"];
$recipient = 'admin@ghims.com';

$subject =  'wwwcontact: (' . $post["form_name"] . ') sent you a message';
$message = '<style> body { font-family: sans-serif; } h2 { color: rgb(230, 107, 0); } </style>';
$message .= '<h2> Subject:' . $post["form_subject"] . '</h2><br>';
if (isset($post['form_phone'])) {
   $message .= '<h3> Phone:' . $post["form_phone"] . '</h3><br>';
}
if (isset($post['admin_name'])) {
   $message .= '<h3> Message To Admin: <b>' . $post["admin_name"] . '</b></h3>';
}
$message .= '<p>' . $post["form_message"] . '</p>';
$headers = 'From:' . $sender . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

if (mail($recipient, $subject, $message, $headers)) {
	echo json_encode(['posreply'=> 'Mail Sent successfully!']);
} else {
	echo json_encode(['negreply'=> 'Mail could not be sent! Pls try again!!']);
}
