#!/usr/bin/env ruby
require 'lsapi'
require 'json'
require 'cgi'
require 'uri'

## constants
$siteConsts = 'consts.rb';

while LSAPI.accept != nil
  require_relative 'main.rb'
end
