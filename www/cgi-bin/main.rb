##
def processSiteConstants()
end

cgi = CGI.new

begin
  respType = 'application/json';
  resp = '';

  ## Check & load configurations
  modt = File.mtime($siteConsts);
  if (not defined?($lmodt)) or (modt > $lmodt)
    $lmodt = modt;
    puts 'loading...';
    load $siteConsts;
    processSiteConstants();
  end

  ## Generate responses
  if ('POST' == ENV['REQUEST_METHOD']) and ('/cgi/calendar.json' == ENV['REQUEST_URI'])
    params = CGI::parse(URI.unescape('start=2019-02-01T00%3A00%3A00%2B05%3A30&end=2019-03-01T00%3A00%3A00%2B05%3A30'));
    puts params;
  elsif ('POST' == ENV['REQUEST_METHOD']) and ('/cgi/timetable.json' == ENV['REQUEST_URI'])
    params = CGI::parse(URI.unescape('thisclass=Pre-KG&start=2019-04-14T00%3A00%3A00%2B05%3A30&end=2019-04-21T00%3A00%3A00%2B05%3A30'));
    #    params = CGI::parse($stdin.read);
    puts params;
##    resp = $timetables[params['class']] if $timetables.has_key?(params['class']);
  end

  ## Finally reutrn
  cgi.out(respType) do
    resp;
  end
rescue
  cgi.out("type" => "text/html", "status" => "500 Internal Server Error") {
    "<html><body><h3>Hit an internal server error... data not available!!</h3></body></html>";
  }
end
